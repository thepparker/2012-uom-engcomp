#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char** argv)
{
    int iNumEntered, scanRes, totalEntered;
    double dArea;

    printf("Hello world!\n");

    printf("Enter the radius of a circle: ");

    //initialise vars
    iNumEntered = 0;
    totalEntered = 0;

    while ((scanRes = scanf("%d", &iNumEntered)) > 0)
    {
        totalEntered++;
        printf("You entered the number %d (%d scanf results)\n", iNumEntered, scanRes);

        dArea = M_PI * pow((double)iNumEntered, 2);

        printf("The area of your circle is %0.2f.", dArea);
        if (totalEntered < 6)
        {
            printf(" Enter another radius!\n");
        }
        else
        {
            printf("\nYou've entered the maximum number of numbers (%d). Exiting\n", totalEntered);

            return 1;
        }
    }

    printf("You didn't enter a number! Time to exit");

    return 0;
}
