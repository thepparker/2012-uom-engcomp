#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define ARRAY_SIZE 15400
#define INT_ARRAY_ENTRIES 7
#define DOUBLE_ARRAY_ENTRIES 4

double simpleDistanceCalc(double prevLatitude, double prevLongitude, double curLatitude, double curLongitude);
double haversineDistanceCalc(double prevLatitude, double prevLongitude, double curLatitude, double curLongitude);

double degToRad(double angle);

int main(int argc, char** argv)
{
    int intData[ARRAY_SIZE][INT_ARRAY_ENTRIES], numread, dataLine, numresult;
    double doubleData[ARRAY_SIZE][DOUBLE_ARRAY_ENTRIES];
    char junklines[128], test;

    /*Data stored in arrays in following sequence:

        intData[line][junk]
        intData[line][yy]
        intData[line][mm]
        intData[line][dd]
        intData[line][hh]
        intData[line][nn]
        intData[line][ss]

        doubleData[line][latitude]
        doubleData[line][longitude]
        doubledata[line][altitude]
        doubleData[line][time]

    */

    numread = 0;
    dataLine = 0;
    while (((numresult = scanf("%[^\n]", junklines)) > 0) && (numread < 6))
    {
        /*printf("Skipping line %d. Read result: %d Data: %s\n", numread, numresult, junklines);*/
        numread++;
        if (numread < 6)
            getchar();
    }

    /*printf("finished skipping. numread: %d\n", numread);*/

    numresult = scanf("%lf,%lf,%d,%lf,%lf,%4d-%2d-%2d,%2d:%2d:%2d",
        &doubleData[dataLine][0], &doubleData[dataLine][1], &intData[dataLine][0],
        &doubleData[dataLine][2], &doubleData[dataLine][3], &intData[dataLine][1],
        &intData[dataLine][2], &intData[dataLine][3], &intData[dataLine][4],
        &intData[dataLine][5], &intData[dataLine][6]);

    /*printf("READ RESULT ON FIRST REAL LINE: %d\n", numresult);*/

    test = getchar();

    /*printf("junk char on end of string: %c", test);*/

    numread++;

    /*printf("GPS trace commences: %02d-%02d-%02d, %02d:%02d:%02d (ARRAY LINE: %d)\n", intData[dataLine][1],
            intData[dataLine][2], intData[dataLine][3], intData[dataLine][4],
            intData[dataLine][5], intData[dataLine][6], dataLine);
    */
    dataLine++;

    while (((numresult = scanf("%lf,%lf,%d,%lf,%lf,%4d-%2d-%2d,%2d:%2d:%2d",
        &doubleData[dataLine][0], &doubleData[dataLine][1], &intData[dataLine][0],
        &doubleData[dataLine][2], &doubleData[dataLine][3], &intData[dataLine][1],
        &intData[dataLine][2], &intData[dataLine][3], &intData[dataLine][4],
        &intData[dataLine][5], &intData[dataLine][6])) > 0)
        && (numread >= 7))
    {
        /*printf("rest of traces: %02d-%02d-%02d, %02d:%02d:%02d. READ RESULT: %d\n", intData[dataLine][1],
            intData[dataLine][2], intData[dataLine][3], intData[dataLine][4],
            intData[dataLine][5], intData[dataLine][6], numresult);*/

        numread++;
        dataLine++;
        getchar();
    }

    /*printf("Reached end. Numread: %d dataLine: %d\n\n\n", numread, dataLine);*/

    int i, yy, mm, dd, hh, nn, ss, firstEnd = 0;
    int durationSeconds, previousIntervalIndex = 0;

    double time, latitude, longitude, speed = 0.0;
    double prevTime, timeDifference, startTime, endTime, durationHours, avgSpeedSimple, avgSpeedHaversine, haversineSimpleDistanceDifference, distanceSimple = 0, distanceHaversine = 0;
    double maxSampleInterval = 0.0, avgSampleInterval = 0.0, intervalDistance = 0.0;

    printf("Stage 1\n=======\n");

    for (i = 0; i < dataLine; i++)
    {
        yy = intData[i][1];
        mm = intData[i][2];
        dd = intData[i][3];
        hh = intData[i][4];
        nn = intData[i][5];
        ss = intData[i][6];

        latitude = doubleData[i][0];
        longitude = doubleData[i][1];
        time = doubleData[i][3];

        if (i > 0)
        {
            distanceSimple += simpleDistanceCalc(doubleData[i-1][0], doubleData[i-1][1], latitude, longitude);
            distanceHaversine += haversineDistanceCalc(doubleData[i-1][0], doubleData[i-1][1], latitude, longitude);

            avgSampleInterval += (time - doubleData[i-1][3]) / (double)(dataLine - 1);

            intervalDistance += haversineDistanceCalc(doubleData[i-1][0], doubleData[i-1][1], latitude, longitude);
            /*printf("Current simple distance accumulation: %d\n", distanceSimple);
            printf("Current haversine distance accumulation: %d\n", distanceHaversine);*/
        }

        if (i == 0)
        {
            printf("GPS trace commences: %04d-%02d-%02d, %02d:%02d:%02d\n", yy, mm, dd, hh, nn, ss);
            startTime = time;
            prevTime = time;
        }
        else if ((timeDifference = (time - prevTime)) > (299.9/(24.0*3600.0)))
        {
            /*printf("Time difference: %0.20f", timeDifference);*/
            speed = intervalDistance / (timeDifference * 24 * 3600) * 3.6;
            if (!firstEnd)
            {
                printf("\nStage 4\n=======\n");
                printf("Period ending %04d-%02d-%02d %02d:%02d:%02d, average speed %5.1f km/hr\n", yy, mm, dd, hh, nn, ss, speed);

                firstEnd = 1;
            }
            else
            {
                printf("Period ending %04d-%02d-%02d %02d:%02d:%02d, average speed %5.1f km/hr\n", yy, mm, dd, hh, nn, ss, speed);
            }

            prevTime = time;
            previousIntervalIndex = i;

            if (timeDifference > maxSampleInterval)
                maxSampleInterval = timeDifference;

            intervalDistance = 0;
        }

        if (i == dataLine - 1)
        {
            /* stage 2 calculations */
            endTime = time;

            durationSeconds = (int)((endTime - startTime) * (24 * 3600));
            durationHours  = (double)durationSeconds / 3600.0;

            printf("\nStage 2\n=======\n");
            printf("GPS trace terminates: %04d-%02d-%02d, %02d:%02d:%02d\n", yy, mm, dd, hh, nn, ss);
            printf("GPS trace samples               : %5d\n", dataLine);
            printf("GPS trace duration              : %5d seconds (%0.1f hours)\n", durationSeconds, durationHours);
            printf("Average interval between samples: %5d seconds\n", (int)(avgSampleInterval * 24 * 3600));
            printf("Maximum interval between samples: %5d seconds\n", (int)(maxSampleInterval * 24 * 3600));

            /* stage 3a calculations */
            avgSpeedSimple = distanceSimple / durationSeconds;

            printf("\nStage 3a\n========\n");
            printf("Total distance travelled using simple rule   : %7d metres\n", (int)distanceSimple);
            printf("Average speed over period of trace           : %7.2f m/sec\n", avgSpeedSimple);

            /* stage 3b calculations */
            haversineSimpleDistanceDifference = distanceSimple - distanceHaversine;
            avgSpeedHaversine = distanceHaversine / durationSeconds;

            printf("\nStage 3b\n========\n");
            printf("Total distance travelled using haversine rule: %7d metres\n", (int)distanceHaversine);
            printf("Average speed over period of trace           : %7.2f m/sec\n", avgSpeedHaversine);
            printf("Difference between simple and haversine      : %7.1e metres\n", haversineSimpleDistanceDifference);
        }
    }

    return 0;
}


double simpleDistanceCalc(double prevLatitude, double prevLongitude, double curLatitude, double curLongitude)
{
    /*printf("Simple distance input co-ords: (%0.4f, %0.4f) prev and (%0.4f, %0.4f) cur\n\n", prevLatitude, prevLongitude, curLatitude, curLongitude);*/

    double earthRadius = 6371000.0;

    double deltaLongitude = degToRad(curLongitude - prevLongitude);
    double theta = degToRad((prevLatitude + curLatitude)/2);
    double deltaLatitude = degToRad(curLatitude - prevLatitude);

    double distance = earthRadius * sqrt(pow(deltaLongitude*cos(theta), 2) + pow(deltaLatitude, 2));

    /*printf("Delta lon: %0.6f, thetaAngle: %0.6f, Delta lat: %0.6f, 1st power: %0.6f, 2nd power: %0.6f, root: %0.6f, distance: %0.6f\n", deltaLongitude, theta, deltaLatitude, pow(deltaLongitude*cos(theta), 2),
            deltaLatitude*deltaLatitude, sqrt((deltaLongitude*cos(theta))*(deltaLongitude*cos(theta)) + deltaLatitude*deltaLatitude), distance);*/

    return distance;
}

double haversineDistanceCalc(double prevLatitude, double prevLongitude, double curLatitude, double curLongitude)
{
    double alpha, earthRadius = 6371000.0;

    alpha = pow(sin(degToRad((curLatitude - prevLatitude) / 2)), 2) + (cos(degToRad(prevLatitude)) * cos(degToRad(curLatitude)) * pow(sin(degToRad((curLongitude - prevLongitude) / 2)), 2));

    return 2 * earthRadius * atan2(degToRad(sqrt(alpha)), degToRad(sqrt(1 - alpha)));
}

double degToRad(double angle)
{
    return (M_PI / 180) * angle;
}
