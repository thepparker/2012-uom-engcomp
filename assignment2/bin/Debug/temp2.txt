Stage 1
========
Number of sound sources: 1
 40.0 meters east,  40.0 meters north, power 0.01000 Watts

Stage 2
========
SPL at (0.0,0.0): 63.2 dB
Point (32, 39) is above the threshold (dB: 80.12)
Point (32, 40) is above the threshold (dB: 80.19)
Point (32, 41) is above the threshold (dB: 80.12)
Point (33, 36) is above the threshold (dB: 80.12)
Point (33, 37) is above the threshold (dB: 80.62)
Point (33, 38) is above the threshold (dB: 81.01)
Point (33, 39) is above the threshold (dB: 81.26)
Point (33, 40) is above the threshold (dB: 81.35)
Point (33, 41) is above the threshold (dB: 81.26)
Point (33, 42) is above the threshold (dB: 81.01)
Point (33, 43) is above the threshold (dB: 80.62)
Point (33, 44) is above the threshold (dB: 80.12)
Point (34, 35) is above the threshold (dB: 80.40)
Point (34, 36) is above the threshold (dB: 81.09)
Point (34, 37) is above the threshold (dB: 81.72)
Point (34, 38) is above the threshold (dB: 82.23)
Point (34, 39) is above the threshold (dB: 82.57)
Point (34, 40) is above the threshold (dB: 82.69)
Point (34, 41) is above the threshold (dB: 82.57)
Point (34, 42) is above the threshold (dB: 82.23)
Point (34, 43) is above the threshold (dB: 81.72)
Point (34, 44) is above the threshold (dB: 81.09)
Point (34, 45) is above the threshold (dB: 80.40)
Point (35, 34) is above the threshold (dB: 80.40)
Point (35, 35) is above the threshold (dB: 81.26)
Point (35, 36) is above the threshold (dB: 82.12)
Point (35, 37) is above the threshold (dB: 82.94)
Point (35, 38) is above the threshold (dB: 83.63)
Point (35, 39) is above the threshold (dB: 84.10)
Point (35, 40) is above the threshold (dB: 84.27)
Point (35, 41) is above the threshold (dB: 84.10)
Point (35, 42) is above the threshold (dB: 83.63)
Point (35, 43) is above the threshold (dB: 82.94)
Point (35, 44) is above the threshold (dB: 82.12)
Point (35, 45) is above the threshold (dB: 81.26)
Point (35, 46) is above the threshold (dB: 80.40)
Point (36, 33) is above the threshold (dB: 80.12)
Point (36, 34) is above the threshold (dB: 81.09)
Point (36, 35) is above the threshold (dB: 82.12)
Point (36, 36) is above the threshold (dB: 83.20)
Point (36, 37) is above the threshold (dB: 84.27)
Point (36, 38) is above the threshold (dB: 85.24)
Point (36, 39) is above the threshold (dB: 85.95)
Point (36, 40) is above the threshold (dB: 86.21)
Point (36, 41) is above the threshold (dB: 85.95)
Point (36, 42) is above the threshold (dB: 85.24)
Point (36, 43) is above the threshold (dB: 84.27)
Point (36, 44) is above the threshold (dB: 83.20)
Point (36, 45) is above the threshold (dB: 82.12)
Point (36, 46) is above the threshold (dB: 81.09)
Point (36, 47) is above the threshold (dB: 80.12)
Point (37, 33) is above the threshold (dB: 80.62)
Point (37, 34) is above the threshold (dB: 81.72)
Point (37, 35) is above the threshold (dB: 82.94)
Point (37, 36) is above the threshold (dB: 84.27)
Point (37, 37) is above the threshold (dB: 85.70)
Point (37, 38) is above the threshold (dB: 87.11)
Point (37, 39) is above the threshold (dB: 88.25)
Point (37, 40) is above the threshold (dB: 88.71)
Point (37, 41) is above the threshold (dB: 88.25)
Point (37, 42) is above the threshold (dB: 87.11)
Point (37, 43) is above the threshold (dB: 85.70)
Point (37, 44) is above the threshold (dB: 84.27)
Point (37, 45) is above the threshold (dB: 82.94)
Point (37, 46) is above the threshold (dB: 81.72)
Point (37, 47) is above the threshold (dB: 80.62)
Point (38, 33) is above the threshold (dB: 81.01)
Point (38, 34) is above the threshold (dB: 82.23)
Point (38, 35) is above the threshold (dB: 83.63)
Point (38, 36) is above the threshold (dB: 85.24)
Point (38, 37) is above the threshold (dB: 87.11)
Point (38, 38) is above the threshold (dB: 89.22)
Point (38, 39) is above the threshold (dB: 91.26)
Point (38, 40) is above the threshold (dB: 92.23)
Point (38, 41) is above the threshold (dB: 91.26)
Point (38, 42) is above the threshold (dB: 89.22)
Point (38, 43) is above the threshold (dB: 87.11)
Point (38, 44) is above the threshold (dB: 85.24)
Point (38, 45) is above the threshold (dB: 83.63)
Point (38, 46) is above the threshold (dB: 82.23)
Point (38, 47) is above the threshold (dB: 81.01)
Point (39, 32) is above the threshold (dB: 80.12)
Point (39, 33) is above the threshold (dB: 81.26)
Point (39, 34) is above the threshold (dB: 82.57)
Point (39, 35) is above the threshold (dB: 84.10)
Point (39, 36) is above the threshold (dB: 85.95)
Point (39, 37) is above the threshold (dB: 88.25)
Point (39, 38) is above the threshold (dB: 91.26)
Point (39, 39) is above the threshold (dB: 95.24)
Point (39, 40) is above the threshold (dB: 98.25)
Point (39, 41) is above the threshold (dB: 95.24)
Point (39, 42) is above the threshold (dB: 91.26)
Point (39, 43) is above the threshold (dB: 88.25)
Point (39, 44) is above the threshold (dB: 85.95)
Point (39, 45) is above the threshold (dB: 84.10)
Point (39, 46) is above the threshold (dB: 82.57)
Point (39, 47) is above the threshold (dB: 81.26)
Point (39, 48) is above the threshold (dB: 80.12)
Point (40, 32) is above the threshold (dB: 80.19)
Point (40, 33) is above the threshold (dB: 81.35)
Point (40, 34) is above the threshold (dB: 82.69)
Point (40, 35) is above the threshold (dB: 84.27)
Point (40, 36) is above the threshold (dB: 86.21)
Point (40, 37) is above the threshold (dB: 88.71)
Point (40, 38) is above the threshold (dB: 92.23)
Point (40, 39) is above the threshold (dB: 98.25)
Point (40, 40) is above the threshold (dB: 1.#J)
Point (40, 41) is above the threshold (dB: 98.25)
Point (40, 42) is above the threshold (dB: 92.23)
Point (40, 43) is above the threshold (dB: 88.71)
Point (40, 44) is above the threshold (dB: 86.21)
Point (40, 45) is above the threshold (dB: 84.27)
Point (40, 46) is above the threshold (dB: 82.69)
Point (40, 47) is above the threshold (dB: 81.35)
Point (40, 48) is above the threshold (dB: 80.19)
Point (41, 32) is above the threshold (dB: 80.12)
Point (41, 33) is above the threshold (dB: 81.26)
Point (41, 34) is above the threshold (dB: 82.57)
Point (41, 35) is above the threshold (dB: 84.10)
Point (41, 36) is above the threshold (dB: 85.95)
Point (41, 37) is above the threshold (dB: 88.25)
Point (41, 38) is above the threshold (dB: 91.26)
Point (41, 39) is above the threshold (dB: 95.24)
Point (41, 40) is above the threshold (dB: 98.25)
Point (41, 41) is above the threshold (dB: 95.24)
Point (41, 42) is above the threshold (dB: 91.26)
Point (41, 43) is above the threshold (dB: 88.25)
Point (41, 44) is above the threshold (dB: 85.95)
Point (41, 45) is above the threshold (dB: 84.10)
Point (41, 46) is above the threshold (dB: 82.57)
Point (41, 47) is above the threshold (dB: 81.26)
Point (41, 48) is above the threshold (dB: 80.12)
Point (42, 33) is above the threshold (dB: 81.01)
Point (42, 34) is above the threshold (dB: 82.23)
Point (42, 35) is above the threshold (dB: 83.63)
Point (42, 36) is above the threshold (dB: 85.24)
Point (42, 37) is above the threshold (dB: 87.11)
Point (42, 38) is above the threshold (dB: 89.22)
Point (42, 39) is above the threshold (dB: 91.26)
Point (42, 40) is above the threshold (dB: 92.23)
Point (42, 41) is above the threshold (dB: 91.26)
Point (42, 42) is above the threshold (dB: 89.22)
Point (42, 43) is above the threshold (dB: 87.11)
Point (42, 44) is above the threshold (dB: 85.24)
Point (42, 45) is above the threshold (dB: 83.63)
Point (42, 46) is above the threshold (dB: 82.23)
Point (42, 47) is above the threshold (dB: 81.01)
Point (43, 33) is above the threshold (dB: 80.62)
Point (43, 34) is above the threshold (dB: 81.72)
Point (43, 35) is above the threshold (dB: 82.94)
Point (43, 36) is above the threshold (dB: 84.27)
Point (43, 37) is above the threshold (dB: 85.70)
Point (43, 38) is above the threshold (dB: 87.11)
Point (43, 39) is above the threshold (dB: 88.25)
Point (43, 40) is above the threshold (dB: 88.71)
Point (43, 41) is above the threshold (dB: 88.25)
Point (43, 42) is above the threshold (dB: 87.11)
Point (43, 43) is above the threshold (dB: 85.70)
Point (43, 44) is above the threshold (dB: 84.27)
Point (43, 45) is above the threshold (dB: 82.94)
Point (43, 46) is above the threshold (dB: 81.72)
Point (43, 47) is above the threshold (dB: 80.62)
Point (44, 33) is above the threshold (dB: 80.12)
Point (44, 34) is above the threshold (dB: 81.09)
Point (44, 35) is above the threshold (dB: 82.12)
Point (44, 36) is above the threshold (dB: 83.20)
Point (44, 37) is above the threshold (dB: 84.27)
Point (44, 38) is above the threshold (dB: 85.24)
Point (44, 39) is above the threshold (dB: 85.95)
Point (44, 40) is above the threshold (dB: 86.21)
Point (44, 41) is above the threshold (dB: 85.95)
Point (44, 42) is above the threshold (dB: 85.24)
Point (44, 43) is above the threshold (dB: 84.27)
Point (44, 44) is above the threshold (dB: 83.20)
Point (44, 45) is above the threshold (dB: 82.12)
Point (44, 46) is above the threshold (dB: 81.09)
Point (44, 47) is above the threshold (dB: 80.12)
Point (45, 34) is above the threshold (dB: 80.40)
Point (45, 35) is above the threshold (dB: 81.26)
Point (45, 36) is above the threshold (dB: 82.12)
Point (45, 37) is above the threshold (dB: 82.94)
Point (45, 38) is above the threshold (dB: 83.63)
Point (45, 39) is above the threshold (dB: 84.10)
Point (45, 40) is above the threshold (dB: 84.27)
Point (45, 41) is above the threshold (dB: 84.10)
Point (45, 42) is above the threshold (dB: 83.63)
Point (45, 43) is above the threshold (dB: 82.94)
Point (45, 44) is above the threshold (dB: 82.12)
Point (45, 45) is above the threshold (dB: 81.26)
Point (45, 46) is above the threshold (dB: 80.40)
Point (46, 35) is above the threshold (dB: 80.40)
Point (46, 36) is above the threshold (dB: 81.09)
Point (46, 37) is above the threshold (dB: 81.72)
Point (46, 38) is above the threshold (dB: 82.23)
Point (46, 39) is above the threshold (dB: 82.57)
Point (46, 40) is above the threshold (dB: 82.69)
Point (46, 41) is above the threshold (dB: 82.57)
Point (46, 42) is above the threshold (dB: 82.23)
Point (46, 43) is above the threshold (dB: 81.72)
Point (46, 44) is above the threshold (dB: 81.09)
Point (46, 45) is above the threshold (dB: 80.40)
Point (47, 36) is above the threshold (dB: 80.12)
Point (47, 37) is above the threshold (dB: 80.62)
Point (47, 38) is above the threshold (dB: 81.01)
Point (47, 39) is above the threshold (dB: 81.26)
Point (47, 40) is above the threshold (dB: 81.35)
Point (47, 41) is above the threshold (dB: 81.26)
Point (47, 42) is above the threshold (dB: 81.01)
Point (47, 43) is above the threshold (dB: 80.62)
Point (47, 44) is above the threshold (dB: 80.12)
Point (48, 39) is above the threshold (dB: 80.12)
Point (48, 40) is above the threshold (dB: 80.19)
Point (48, 41) is above the threshold (dB: 80.12)
Points above threshold: 213
Stage 3
========
Points sampled: 10201
Above 80.0 dB: 2.1%

Stage 4
========
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666   
666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666 
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
666666666666666666666666666666666666666  66666666666666666666666666666666666666666666666666666666666
66666666666666666666666666666                      6666666666666666666666666666666666666666666666666
6666666666666666666666666        77777777777777        666666666666666666666666666666666666666666666
6666666666666666666666      777777777777777777777777      666666666666666666666666666666666666666666
66666666666666666666     777777777777777777777777777777     6666666666666666666666666666666666666666
666666666666666666    777777777777777777777777777777777777    66666666666666666666666666666666666666
6666666666666666     77777777777777777777777777777777777777     666666666666666666666666666666666666
666666666666666    777777777777777777777777777777777777777777    66666666666666666666666666666666666
66666666666666    77777777777777777777777777777777777777777777    6666666666666666666666666666666666
6666666666666    7777777777777777777777777777777777777777777777    666666666666666666666666666666666
666666666666    7777777777777777777777    7777777777777777777777    66666666666666666666666666666666
666666666666   7777777777777777777  88888888  7777777777777777777   66666666666666666666666666666666
66666666666    77777777777777777  888888888888  77777777777777777    6666666666666666666666666666666
66666666666   77777777777777777 8888888888888888 77777777777777777   6666666666666666666666666666666
66666666666   77777777777777777 88888 9999 88888 77777777777777777   6666666666666666666666666666666
66666666666   77777777777777777 88888 9999 88888 77777777777777777   6666666666666666666666666666666
66666666666   77777777777777777 8888888888888888 77777777777777777   6666666666666666666666666666666
66666666666    77777777777777777  888888888888  77777777777777777    6666666666666666666666666666666
666666666666   7777777777777777777  88888888  7777777777777777777   66666666666666666666666666666666
666666666666    7777777777777777777777    7777777777777777777777    66666666666666666666666666666666
6666666666666    7777777777777777777777777777777777777777777777    666666666666666666666666666666666
66666666666666    77777777777777777777777777777777777777777777    6666666666666666666666666666666666
666666666666666    777777777777777777777777777777777777777777    66666666666666666666666666666666666
6666666666666666     77777777777777777777777777777777777777     666666666666666666666666666666666666
666666666666666666    777777777777777777777777777777777777    66666666666666666666666666666666666666
66666666666666666666     777777777777777777777777777777     6666666666666666666666666666666666666666
6666666666666666666666      777777777777777777777777      666666666666666666666666666666666666666666
6666666666666666666666666        77777777777777        666666666666666666666666666666666666666666666
66666666666666666666666666666                      6666666666666666666666666666666666666666666666666
666666666666666666666666666666666666666  66666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
6666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666666
contour end height: 0, countor end width: 100
