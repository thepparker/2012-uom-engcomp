#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define REFERENCE_PRESSURE 20e-6
#define REFERENCE_POWER 1e-12

#define ABSORPTION_COEFF 0.5
#define SURFACE_ABSORPTION_CONSTANT 2.0
#define DIRECTIVITY_FACTOR 2.0

#define MAX_SOUND_SOURCES_AND_WALLS 1000

#define HEARING_DAMAGE_THRESHOLD 80.0

#define X_GRID_SIZE 100
#define Y_GRID_SIZE 100

//define and declare our structs
struct xyPoint
{
    double x;
    double y;
};

struct xyLine
{
    struct xyPoint start;
    struct xyPoint finish;
};

struct sourceParameters
{
    struct xyPoint sourcePoint; // the x,y co-ords of our sound source

    double wattage;

};

struct wallParameters
{
    struct xyLine wallLine;

    double soundReduction;
};

struct completeSoundEnvironment
{
    int numSources;
    int numWalls;

    struct sourceParameters sourceData[MAX_SOUND_SOURCES_AND_WALLS];
    struct wallParameters wallData[MAX_SOUND_SOURCES_AND_WALLS];
};

void addDecibel(double *db1, const double db2); //adds db2 to the value pointed to by db1

double soundPowerLevel(double sourceWattage); //returns the sound power level of the source given the wattage

double decibelLevelRelative(struct sourceParameters *sourceData, struct xyPoint *point); //calculate the decibel level relative to a source at a given point
double decibelLevelAtPoint(struct completeSoundEnvironment *currentEnvironment, struct xyPoint *point, int useWalls); //calculates the decibel level at a point from all sources and optionally including walls
double absolute(double num);

int soundContourConvert(double soundLevel); //convert the decibel into a single digit for graphical represenation

int pointInWallEnd(struct xyLine *wall, struct xyPoint *point); //whether the point is in the end of a wall
int checkPointWallCollision(struct xyLine *wall, struct xyLine *sourceAndPoint); //whether the point/source interset with a wall, and hence whether the sound level is reduced

void printStageOne(struct completeSoundEnvironment *currentEnvironment);
void printStageTwo(struct completeSoundEnvironment *currentEnvironment);
void printStageThree(struct completeSoundEnvironment *currentEnvironment);
void printStageFour(struct completeSoundEnvironment *currentEnvironment);
void printStageFive(struct completeSoundEnvironment *currentEnvironment);
void printStageSix(struct completeSoundEnvironment *currentEnvironment);

int main(int argc, char **argv)
{
    int sourceIdx = 0, wallIdx = 0;

    struct completeSoundEnvironment soundEnvironment;

    while (scanf("%lf %lf %lf", &soundEnvironment.sourceData[sourceIdx].sourcePoint.x,
                 &soundEnvironment.sourceData[sourceIdx].sourcePoint.y,
                 &soundEnvironment.sourceData[sourceIdx].wattage) > 0)
    {
        sourceIdx++;
        getchar(); // trash the \n
    }

    soundEnvironment.numSources = sourceIdx;
    getchar(); // trash the # character that separates walls and sound sources in the input file

    while (scanf("%lf %lf %lf %lf %lf", &soundEnvironment.wallData[wallIdx].wallLine.start.x,
                 &soundEnvironment.wallData[wallIdx].wallLine.start.y,
                 &soundEnvironment.wallData[wallIdx].wallLine.finish.x,
                 &soundEnvironment.wallData[wallIdx].wallLine.finish.y,
                 &soundEnvironment.wallData[wallIdx].soundReduction) > 0)
    {
        wallIdx++;
        getchar(); // trash the \n
    }

    soundEnvironment.numWalls = wallIdx;

    printStageOne(&soundEnvironment);
    printStageTwo(&soundEnvironment);
    printStageThree(&soundEnvironment);
    printStageFour(&soundEnvironment);
    printStageFive(&soundEnvironment);

    return 0;
}

void printStageOne(struct completeSoundEnvironment *currentEnvironment)
{
    printf("\nStage 1\n=======\nNumber of sound sources: %d\n", currentEnvironment->numSources);

    int i;
    for (i = 0; i < currentEnvironment->numSources; i++)
    {
        struct xyPoint sourcePoint = currentEnvironment->sourceData[i].sourcePoint;
        printf("%5.1f meters east, %5.1f meters north, power %0.5f Watts\n", sourcePoint.x,
               sourcePoint.y, currentEnvironment->sourceData[i].wattage);
    }
}

void printStageTwo(struct completeSoundEnvironment *currentEnvironment)
{
    struct xyPoint origin = { 0, 0 };

    double splAtOrigin = decibelLevelAtPoint(currentEnvironment, &origin, 0);

    printf("\nStage 2\n=======\nSPL at (0.0,0.0): %0.1f dB\n", splAtOrigin);
}

void printStageThree(struct completeSoundEnvironment *currentEnvironment)
{
    int i, j, pointsSampled = 0, pointsAboveThreshold = 0;

    for (i = 0; i <= X_GRID_SIZE; i++)
    {
        for (j = 0; j <= Y_GRID_SIZE; j++)
        {
            struct xyPoint samplePoint = { i, j };

            double dbAtPoint = decibelLevelAtPoint(currentEnvironment, &samplePoint, 0);

            if (dbAtPoint > HEARING_DAMAGE_THRESHOLD)
            {
                pointsAboveThreshold++;
            }

            pointsSampled++;
        }
    }

    double aboveThresholdPercentage = 1.0 * pointsAboveThreshold / pointsSampled * 100;

    printf("\nStage 3\n=======\nPoints sampled: %d\nAbove 80.0 dB: %0.1f%%\n", pointsSampled, aboveThresholdPercentage);
}

void printStageFour(struct completeSoundEnvironment *currentEnvironment)
{
    int i, j;

    printf("\nStage 4\n=======\n");

    for (j = Y_GRID_SIZE; j > 0;)
    {
        for (i = 0; i < X_GRID_SIZE; i++)
        {
            struct xyPoint samplePoint = { (double)i + 0.5, j-1 };

            double dbAtPoint = decibelLevelAtPoint(currentEnvironment, &samplePoint, 0);

            int contourAtPoint = soundContourConvert(dbAtPoint);

            if (contourAtPoint > 0)
                printf("%d", contourAtPoint);
            else
                printf(" ");
        }

        printf("\n"); //go to next line of plot

        j -= 2;
    }
}

void printStageFive(struct completeSoundEnvironment *currentEnvironment)
{
    int i, j;

    printf("\nStage 5\n=======\nNumber of barriers in place: %d\n", currentEnvironment->numWalls);

    for (j = Y_GRID_SIZE; j > 0;)
    {
        for (i = 0; i < X_GRID_SIZE; i++)
        {
            struct xyPoint samplePoint = { (double)i + 0.5, j-1 };

            double dbAtPoint = decibelLevelAtPoint(currentEnvironment, &samplePoint, 1); //decibel level @ a point taking walls into consideration

            if (dbAtPoint == -1) // point is in one of the ends of the wall
                printf("*");
            else
            {
                int contourAtPoint = soundContourConvert(dbAtPoint);

                if (contourAtPoint > 0)
                    printf("%d", contourAtPoint);
                else if (contourAtPoint == -1)
                    printf("-");
                else
                    printf(" ");
            }

        }

        printf("\n");

        j -= 2;
    }
}

int soundContourConvert(double soundLevel)
{
    if (soundLevel < 0)
        return -1;

    if (soundLevel >= 90)
        return 9;

    int i;
    for (i = 0; i <= 10; i++)
    {
        if (((i*10) < soundLevel) && (soundLevel < (i*10 + 5)))
            return i;
    }

    return 0;
}

double decibelLevelAtPoint(struct completeSoundEnvironment *currentEnvironment, struct xyPoint *point, int useWalls)
{
    int i, j;
    double splAtPoint = 0;

    if (!useWalls)
    {
        for (i = 0; i < currentEnvironment->numSources; i++)
        {
            addDecibel(&splAtPoint, decibelLevelRelative(&currentEnvironment->sourceData[i], point));
        }

        return splAtPoint;
    }
    else
    {
        for (i = 0; i < currentEnvironment->numWalls; i++) //loop through the walls once to see if the point is in the walls before doing all walls AND sources
        {
            if (pointInWallEnd(&currentEnvironment->wallData[i].wallLine, point))
                return -1;
        }

        for (j = 0; j < currentEnvironment->numSources; j++)
        {
            double soundReduction = 0;
            for (i = 0; i < currentEnvironment->numWalls; i++)
            {
                struct xyLine sourceAndPoint;

                sourceAndPoint.start.x = currentEnvironment->sourceData[j].sourcePoint.x;
                sourceAndPoint.start.y = currentEnvironment->sourceData[j].sourcePoint.y;
                sourceAndPoint.finish.x = point->x;
                sourceAndPoint.finish.y = point->y;

                if (checkPointWallCollision(&currentEnvironment->wallData[i].wallLine, &sourceAndPoint)) //DOES THE SOURCE/POINT INTERSECT WITH A WALL?
                {
                    soundReduction += currentEnvironment->wallData[i].soundReduction;
                }
            }

            addDecibel(&splAtPoint, decibelLevelRelative(&currentEnvironment->sourceData[j], point) - soundReduction);
        }
        return splAtPoint;
    }
}

double decibelLevelRelative(struct sourceParameters *sourceData, struct xyPoint *point)
{
    double pointDistance = sqrt(pow(point->x - sourceData->sourcePoint.x, 2) + pow(point->y - sourceData->sourcePoint.y, 2));

    double sourcePowerLevel = soundPowerLevel(sourceData->wattage);

    double surfaceAbsorption = (SURFACE_ABSORPTION_CONSTANT + ABSORPTION_COEFF) * M_PI * pow(pointDistance, 2);

    double firstCalc = DIRECTIVITY_FACTOR / (4 * M_PI * pow(pointDistance, 2));

    return sourcePowerLevel + 10.0 * log10(firstCalc + 4 / surfaceAbsorption);
}

double soundPowerLevel(double sourceWattage)
{
    return 10.0 * log10(sourceWattage / REFERENCE_POWER);
}

void addDecibel(double *db1, const double db2)
{
    double splFirstCalc = pow(10, *db1/10);
    double splSecondCalc = pow(10, db2/10);

    *db1 = 10.0 * log10(splFirstCalc + splSecondCalc);
}

int checkPointWallCollision(struct xyLine *wall, struct xyLine *sourceAndPoint)
{
    /*need to establish two lines
    1 line between the start and end point of the wall, and 1 between the source point and our display point

    Using logic from http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/ by Paul Bourke

    Let the wall be x1, y1 and x2, y2
    Let the sound sourcePoint be x3, y3
    Let the point be x4, y4
    */

    double x1 = wall->start.x, y1 = wall->start.y;
    double x2 = wall->finish.x, y2 = wall->finish.y;

    double x3 = sourceAndPoint->start.x, y3 = sourceAndPoint->start.y;

    double x4 = sourceAndPoint->finish.x, y4 = sourceAndPoint->finish.y;

    double UabDemoninator = (y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1);

    if ((int)UabDemoninator == 0) //the wall is parallel to the source and point
    {
        return 0;
    }

    double UaNumerator = (x4 - x3)*(y1 - y3) - (y4 - y3)*(x1 - x3);

    double UbNumerator = (x2 - x1)*(y1-y3) - (y2 - y1) * (x1 - x3);

    double Ua = UaNumerator / UabDemoninator;
    double Ub = UbNumerator / UabDemoninator;

    if (((int)UaNumerator == 0) && ((int)UbNumerator == 0)) //the wall and the source/point are coincident
    {
        return 1;
    }

    if ((Ua >= 0) && (Ua <= 1) && (Ub >= 0) && (Ub <= 1))
    {
        return 1;
    }

    return 0;
}

int pointInWallEnd(struct xyLine *wall, struct xyPoint *point)
{
    double wallX1 = wall->start.x, wallY1 = wall->start.y;
    double wallX2 = wall->finish.x, wallY2 = wall->finish.y;

    double pointX = point->x, pointY = point->y;

    if ((absolute(pointX - wallX1) <= 0.5) && (absolute(pointY - wallY1) <= 1))
        return 1;
    if ((absolute(pointX - wallX2) <= 0.5) && (absolute(pointY - wallY2) <= 1))
        return 1;

    return 0;
}

double absolute(double num)
{
    return sqrt(num * num);
}
